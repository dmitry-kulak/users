# users

Простенькое приложение, отображающее пользователей c [jsonplaceholder/users](https://jsonplaceholder.typicode.com/users/) и сортирующее их. Также можно их отредактировать (но только локально).

Познакомился с redux-toolkit и redux-saga (зачем, если есть rtk-query?), Formik (зачем, если есть React Hook Form?).

[Деплой](https://dk-users.netlify.app/).

## Стэк

* React
* Typescript
* Redux
* Redux-saga
* Redux-toolkit
* Axios
* CSS-modules
* SASS
* Formik
* yup
* React Loading Skeleton
